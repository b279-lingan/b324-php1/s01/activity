<?php require_once "./code.php";  ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ACTIVITIES S01</title>
</head>
<body>

//ACTIVITY 1

<h3>Full Address</h3>
	<p><?php echo  getFullAddress('Philippines', 'Metro Manila', 'Quezon City', '3F Caswynn Bldg., Timog Avenue'); ?></p>
	<p><?php echo  getFullAddress('Philippines', 'Metro Manila', 'Makati City', '3F Enzo Bldg., Buendia Avenue'); ?></p>

//ACTIVITY 2

<h3>Letter-Based Grading</h3>
	<p><?php echo  getLetterGrade(99); ?></p>
	<p><?php echo  getLetterGrade(44); ?></p>
	<p><?php echo  getLetterGrade(57); ?></p>
	<p><?php echo  getLetterGrade(79); ?></p>
	<p><?php echo  getLetterGrade(89); ?></p>
	<p><?php echo  getLetterGrade(91); ?></p>
	<p><?php echo  getLetterGrade(95); ?></p>

</body>
</html>
