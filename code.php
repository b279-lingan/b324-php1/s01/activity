<?php 

//ACTIVITY 1

//Full Address

function getFullAddress($country, $city, $province, $specificAddress){
		return "$specificAddress, $city, $province, $country";
}

//ACTIVITY 2

//Letter-Based Grading

function getLetterGrade($grade){

	$let;
	switch($grade)
	{
		case $grade >= 98 && $grade <= 100:
			$let =  "A+";
			break;
		case $grade >= 95 && $grade <= 97:
			$let =  "A";
			break;
		case $grade >= 92 && $grade <= 94:
			$let =  "A-";
			break;
		case $grade >= 89 && $grade <= 91:
			$let =  "B+";
			break;
		case $grade >= 86 && $grade <= 88:
			$let =  "B";
			break;
		case $grade >= 83 && $grade <= 85:
			$let =  "B-";
			break;
		case $grade >= 80 && $grade <= 82:
			$let =  "C+";
			break;
		case $grade >= 77 && $grade <= 79:
			$let =  "C";
			break;
		case $grade >= 75 && $grade <= 76:
			$let =  "C-";
			break;
		case $grade < 75 :
			$let =  "D";
			break;

		default:
		return "Invalid Grade Input";
	}

	return "$grade is equivalent to $let";

}

 ?>